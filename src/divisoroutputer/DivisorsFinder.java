package divisoroutputer;

import java.util.ArrayList;

/**
 *
 * @author jarek
 */
public class DivisorsFinder {

    private int operand;
    private final ArrayList<Integer> divisors;

    public DivisorsFinder() {
        this.divisors = new ArrayList<>();
    }

    public void setOperand(int operand) throws IllegalArgumentException {
        if (operand <= 0) {
            throw new IllegalArgumentException("Operand has to be > 0");
        }
        this.operand = operand;
    }

    public ArrayList<Integer> getDivisors() {
        this.divisors.clear();
        if (this.operand > 1) {
            for (int i = 1; i <= this.operand / 2; ++i) {
                if (this.operand % i == 0) {
                    this.divisors.add(i);
                }
            }
            this.divisors.add(this.operand);
        } else {
            this.divisors.add(1);
        }

        return this.divisors;
    }

}
