package divisoroutputer;

import java.util.Scanner;

/**
 *
 * @author jarek
 */
public class DivisorOutputer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String answer;
        int userNumber;
        DivisorsFinder divisorsFinder = new DivisorsFinder();
        try (Scanner scanner = new Scanner(System.in)) {
            do {
                userNumber = getUserNumber(scanner);
                divisorsFinder.setOperand(userNumber);
                System.out.println("Divisors of " + userNumber + " are: " + divisorsFinder.getDivisors());
                System.out.print("New number? (y/n) [y]: ");
                answer = scanner.next();
            } while (answer.toLowerCase().equals("y"));
        }
    }

    private static int getUserNumber(Scanner scanner) {
        int userNumber;
        do {
            System.out.print("Enter a natural number: ");
            while (!scanner.hasNextInt()) {
                System.out.print("Enter a natural number: ");
                scanner.next();
            }
            userNumber = scanner.nextInt();
        } while (userNumber <= 0);

        return userNumber;
    }

}
